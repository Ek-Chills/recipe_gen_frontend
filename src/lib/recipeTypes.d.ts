type Recipe = {
  recipe:{
    foodDescription:string,
    steps:{
      stepNo:number,
      step:string
    }[]
  }
}


type SIngleRecipe = {
  id:number;
  name:string;
  description:string;
  steps:{
    stepNo:number;
    step:string[]
  }[]
}

type AllRecipes = SIngleRecipe[]