import { HttpClient } from "@angular/common/http";
import { Injectable, Optional } from "@angular/core";
import { baseUrlAuth } from "../lib/url";
import { Router } from "@angular/router";

@Injectable({
  providedIn:'root'
})

export class AuthService{
  constructor( @Optional() private http:HttpClient, @Optional() private router:Router){}

  isRegistering = false
  isLoggingIn = false

  registerUser(email:string,password:string, verificationUrl:string) {
    this.isRegistering = true
    this.http.post(baseUrlAuth + '/register', {
      email,
      password,
      verificationUrl
    }).subscribe({
      next:async(res) => {
        console.log(res);
        await this.router.navigate(['/verify-email'])
        return res
      },
      error:(err) => {
        console.log(err);
        this.isRegistering = false
      },
      complete:() => {
        this.isRegistering = false
      }
    })
  }

  loginUser(email:string,password:string) {
    this.isLoggingIn = true
    this.http.post<{email:string, userId:string | number, accessToken:string}>(baseUrlAuth + '/login', {
      email,
      password,

    }).subscribe({
      next:async(res) => {
        console.log('login res', res);
        this.setUserDetailsAndAccessToken(res.email, res.accessToken, res.userId)
        await this.router.navigate(['recipes'])
        return res
      },
      error:(err) => {
        console.log(err);
        this.isLoggingIn = false
      },
      complete:() => {
        this.isLoggingIn = false
      }
    })
  }

  setUserDetailsAndAccessToken(email:string, accessToken:string, userId:string | number) {
    localStorage.setItem('userDetails', JSON.stringify({email, userId}))
    localStorage.setItem('accessToken', accessToken)
  }

  isUserPresent() {
    const isUserDetails = !!localStorage.getItem('userDetails')
    return isUserDetails
  }

  getUserDetails() {
    return JSON.parse(localStorage.getItem('userDetails')!) as {email:string, userId:string | number}
  }

  logoutUser() {
    localStorage.removeItem('accessToken')
    localStorage.removeItem('userDetails')
    this.router.navigate(['/login'])
  }
  // isLoggedIn(){
  //   return !!localStorage.getItem('token');
  // }

  // logout(){
    
  }
