import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, GuardResult, MaybeAsync, Router, RouterStateSnapshot } from "@angular/router";
import { AuthService } from "./authService.service";

@Injectable({
  providedIn: "root"
})

export class AuthGuard implements CanActivate {
  constructor (private authService:AuthService, private router:Router) {}
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): MaybeAsync<GuardResult> {
    if(this.authService.isUserPresent()) {
      return true
    } else {
     return this.router.navigate(["/login"])
    }
  }
}