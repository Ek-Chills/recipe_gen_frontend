import { Component } from '@angular/core';
import { RouterLink } from '@angular/router';
import { AvatarDropdownComponent } from '../avatar-dropdown/avatar-dropdown.component';

@Component({
  selector: 'app-navbar',
  standalone: true,
  imports: [RouterLink, AvatarDropdownComponent],
  templateUrl: './navbar.component.html',
  styleUrl: './navbar.component.css'
})
export class NavbarComponent {
  isDropdownOpen = false;

  toggleDropDown() {
    this.isDropdownOpen = !this.isDropdownOpen;
  }
}
