import { CommonModule } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { AuthService } from '../../services/authService.service';


@Component({
  selector: 'app-register-page',
  standalone: true,
  imports: [ReactiveFormsModule, CommonModule],
  templateUrl: './register-page.component.html',
  styleUrl: './register-page.component.css'
})
export class RegisterPageComponent  {

  constructor(private authService:AuthService) {}

  registerForm = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required]),
    confirmPassword: new FormControl('', [Validators.required])
  })

  get email() {
    return this.registerForm.get('email');
  }
  get password() {
    return this.registerForm.get('password');
  }
  get confirmPassword() {
    return this.registerForm.get('confirmPassword');
  }
  get isRegistering() {
    return this.authService.isRegistering;
  }

  async handleFormSubmit() {
    console.log('submitted');
    
    if(!this.email?.value || !this.password?.value || !this.confirmPassword?.value) return;
    if(this.password.value !== this.confirmPassword.value) return;
    this.authService.registerUser(this.email.value, this.password.value, 'https://recipe-gen-frontend.vercel.app/activate')
  }

}
