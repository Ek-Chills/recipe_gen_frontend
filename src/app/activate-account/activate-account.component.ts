import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { baseUrlAuth } from '../../lib/url';

@Component({
  selector: 'app-activate-account',
  standalone: true,
  imports: [],
  templateUrl: './activate-account.component.html',
  styleUrl: './activate-account.component.css'
})
export class ActivateAccountComponent implements OnInit {

  isActivating = false;
  constructor(private route:ActivatedRoute, private http:HttpClient, private router:Router) {}

  ngOnInit(): void {
    const token = this.route.snapshot.paramMap.get('token');
    this.isActivating = true;
    this.http.post<{success:boolean}>(`${baseUrlAuth}/activate`, {token:token}).subscribe(
      {
        next:(res)=>{

        console.log('from activated', res);
        if(res.success) {
          this.router.navigate(['/login']);
        }
        },
        error:(err)=>{
          console.log(err);
          this.isActivating = false;
        },
        complete:() => {
          this.isActivating = false;
        }
      }
    )
    console.log(token);
    
    // console.log(token);
  }
}
