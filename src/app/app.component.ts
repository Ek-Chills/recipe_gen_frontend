import { Component, OnInit, ViewChild } from '@angular/core';
import { ChildrenOutletContexts, RouterOutlet } from '@angular/router';
import { LoginPageComponent } from './login-page/login-page.component';
import { ToastContainerDirective, ToastrService } from 'ngx-toastr';
import { slideInAnimation } from './animations';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, LoginPageComponent],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css',
  animations:[
    slideInAnimation
  ]
})
export class AppComponent implements OnInit {
  title = 'recipe-generator-frontend';
  @ViewChild(ToastContainerDirective, { static: true }) toastContainer?: ToastContainerDirective;

  constructor(private toastrService: ToastrService, private contexts:ChildrenOutletContexts) {}

  ngOnInit(): void {
    
    this.toastrService.overlayContainer = this.toastContainer;
  }

  getRouteAnimationData() {
    return this.contexts.getContext('primary')?.route?.snapshot?.data?.['animation'];
  }
}
