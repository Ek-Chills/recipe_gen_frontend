import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/authService.service';

@Component({
  selector: 'app-avatar-dropdown',
  standalone: true,
  imports: [],
  templateUrl: './avatar-dropdown.component.html',
  styleUrl: './avatar-dropdown.component.css',
})
export class AvatarDropdownComponent implements OnInit {
  isDropdownOpen = false;
  userEmail = '';

  constructor(private authService: AuthService) {
    
  }
  ngOnInit(): void {
    this.userEmail = this.authService.getUserDetails().email;
  }
  toggleDropDown() {
    this.isDropdownOpen = !this.isDropdownOpen;
  }

  logout() {
    this.authService.logoutUser()
  }

}
