import { Routes } from '@angular/router';
import { LoginPageComponent } from './login-page/login-page.component';
import { RegisterPageComponent } from './register-page/register-page.component';
import { VerifyEmailComponent } from './verify-email/verify-email.component';
import { ActivateAccountComponent } from './activate-account/activate-account.component';
import { RecipesComponent } from './recipes/recipes.component';
import { CreateRecipeComponent } from './create-recipe/create-recipe.component';
import { AuthGuard } from '../services/auth-guard';
import { HomeComponent } from './home/home.component';
import { SingleRecipeComponent } from './single-recipe/single-recipe.component';
import { animation } from '@angular/animations';

export const routes: Routes = [
  { path: '', redirectTo: '/recipes', pathMatch: 'full' }, // Redirect to /recipes
  { path: 'login', component: LoginPageComponent },
  { path: 'register', component: RegisterPageComponent },
  { path: 'verify-email', component: VerifyEmailComponent },
  { path: 'activate/:token', component: ActivateAccountComponent },
  { path: 'recipes', component: RecipesComponent, canActivate: [AuthGuard], data:{animation:'allRecipesPage'} },
  {
    path: 'recipes/create',
    component: CreateRecipeComponent,
    canActivate: [AuthGuard],
    data:{
      animation:'generateRecipePage'
    }
  },
  {
    path: 'recipes/:id',
    component: SingleRecipeComponent,
    canActivate: [AuthGuard],
  },
];
