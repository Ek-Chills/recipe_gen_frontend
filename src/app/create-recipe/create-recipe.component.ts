import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { NavbarComponent } from '../navbar/navbar.component';
import { AuthService } from '../../services/authService.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { baseUrlRecipe } from '../../lib/url';


@Component({
  selector: 'app-create-recipe',
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule, NavbarComponent],
  templateUrl: './create-recipe.component.html',
  styleUrl: './create-recipe.component.css'
})
export class CreateRecipeComponent  {

  @ViewChild('inputRef') inputElement?:ElementRef<HTMLInputElement>;
  isGenerating = false;
  isSaving = false;
  name:string = '';
  recipeError = {
    isError:false,
    msg:''
  }
  results:Recipe = {recipe:{
    foodDescription:'',
    steps:[]
  }}

  searchForm = new FormGroup({
    query: new FormControl('', Validators.required)
  })

  constructor(private http:HttpClient, private authService:AuthService, private router:Router,  private toastr:ToastrService) {

  }



  generateRecipe() {
    if(!this.inputElement?.nativeElement.value) return;
    const prompt = this.inputElement?.nativeElement.value;
    this.fetchRecipe(prompt);
  }

  fetchRecipe(recipePrompt:string) {
    this.isGenerating = true;
    this.recipeError = {
      isError:false,
      msg:''
    }
    this.http.post(`${baseUrlRecipe}/createRecipe`, {prompt:recipePrompt})
    .subscribe({
      next:(res) => {
        this.name = this.inputElement?.nativeElement.value!
        console.log(res);
        this.inputElement!.nativeElement.value = '';
        this.results = res as Recipe;
      },
      error:(err) => {
        console.log(err);
        this.isGenerating = false;
        this.recipeError = {
          isError:true,
          msg:err.error.msg
        }
        this.toastr.error('Error generating recipe!');
      }, complete:() => {
        this.isGenerating = false;
      }
    })
  }

  saveRecipe() {
    this.isSaving = true;
    if(!this.results.recipe.foodDescription || !this.results.recipe.steps.length) return;
    this.http.post(`${baseUrlRecipe}/save/${this.authService.getUserDetails().userId}`, {name:this.name, description:this.results.recipe.foodDescription, steps:this.results.recipe.steps}).subscribe({
      next:(res) => {
        this.toastr.success('Recipe saved successfully!');
        this.router.navigate(['/recipes']);
      },
      complete:() => {
        this.isSaving = false;
      },
      error:(err) => {
        this.isSaving = false;
        console.log(err);
        this.toastr.error('Error saving recipe!');
      }
    })
  }
}
