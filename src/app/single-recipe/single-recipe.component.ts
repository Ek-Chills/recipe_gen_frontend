import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavbarComponent } from '../navbar/navbar.component';
import { baseUrlRecipe } from '../../lib/url';

@Component({
  selector: 'app-single-recipe',
  standalone: true,
  imports: [NavbarComponent],
  templateUrl: './single-recipe.component.html',
  styleUrl: './single-recipe.component.css'
})
export class SingleRecipeComponent implements OnInit {
  isFetching = false;
  result?:SIngleRecipe;
constructor(private http:HttpClient, private route:ActivatedRoute) {}

  ngOnInit(): void {
    this.isFetching = true;
    this.http.get<SIngleRecipe>(`${baseUrlRecipe}/getSingleRecipe/${this.route.snapshot.params['id']}`).subscribe({
      next:(res)=>{
        console.log(res);
        this.result = res;
      },
      error:(err)=>{
        console.log(err);
      },
      complete:() => {
        this.isFetching = false;
      }
    })
  }
  
  
//   recipe =
}
