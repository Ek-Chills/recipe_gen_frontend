import { Component, OnInit } from '@angular/core';
import { NavbarComponent } from '../navbar/navbar.component';
import { HttpClient } from '@angular/common/http';
import { LoadingSkeletonComponent } from '../loading-skeleton/loading-skeleton.component';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../../services/authService.service';
import { NgClass } from '@angular/common';
import { RouterLink } from '@angular/router';
import { baseUrlRecipe } from '../../lib/url';


@Component({
  selector: 'app-recipes',
  standalone: true,
  imports: [NavbarComponent, LoadingSkeletonComponent, NgClass, RouterLink],
  templateUrl: './recipes.component.html',
  styleUrl: './recipes.component.css'
})
export class RecipesComponent implements OnInit {
  allRecipes:AllRecipes = []
  isFetching = false;
  isDeleting = false;
 constructor (private http:HttpClient, private authService:AuthService ) {

 } 

 ngOnInit(): void {
   this.fetchRecipes()
 }

fetchRecipes () {
  this.isFetching = true;
  this.http.get<AllRecipes>(`${baseUrlRecipe}/getRecipe/${this.authService.getUserDetails().userId}`).subscribe({
    next:(res) => {
      console.log(res)
      this.allRecipes = res
    },
    error:(err) => {
      console.log(err);
      this.isFetching = false;
    },
    complete:() => {
      this.isFetching = false;
    }
  })
}

deleteRecipe(recipeId:number){
  this.isDeleting = true;
  this.http.delete(`${baseUrlRecipe}/deleteRecipe/${recipeId}`).subscribe({
    next:(res) => {
      console.log(res)
      this.fetchRecipes()
    },
    error:(err) => {
      console.log(err);
    },
    complete:() => {
      this.isDeleting = false
    }
  })
}
}
